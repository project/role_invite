=== SUMMARY ===

Assigning roles and responsibilities while inviting friends is very important.
This module gives you the additional flexibility of assigning a role for 
the invitee right at the time of sending the invitation.

=== REQUIREMENTS ===

* Invite module http://drupal.org/project/invite
* Token module http://drupal.org/project/token


=== INSTALLATION ===

1. Copy the role invite module to your modules directory and enable it on the Modules
   page (admin/build/modules).

2. In the permissions page (admin/user/permissions), assign the permissions that you 
   want to grant for that particular role. 

=== USAGE ===

To invite a friend :

1. Click the 'Invite your friends' link.
3. Fill in the e-mail address(es) of the person(s) you would like to invite,
   and add a personal message.
4. Select role(s) with which you would like to invite.
5. Press submit.

=== TROUBLESHOOTING ===

While sendng invitation requests, different types of roles are available.
Now if you think one role is superior to another and want to support an hirarcy
of roles, add your code to the function _get_roles() in role_invite.module.


== CREDITS ==

Original author:
  Raghunath Akula